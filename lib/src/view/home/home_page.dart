import 'package:dogapp/src/controller/home_controller.dart';
import 'package:dogapp/src/model/breed_model.dart';
import 'package:dogapp/src/view/breed_details/breed_details_screen.dart';
import 'package:dogapp/src/view/favorites/favorites_screen.dart';
import 'package:dogapp/src/view/shared/custom_card_widget.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _controller = HomeController();
  late List<BreedModel> favorites;
  @override
  initState() {
    _getFavorites();
    super.initState();
  }

  _getFavorites() async {
    favorites = await _controller.getFavorites();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Breeds'),
        actions: [
          IconButton(
              icon: const Icon(Icons.favorite_border_outlined),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => FavoritesScreen())).then(
                  (value) => setState(() {
                    _getFavorites();
                  }),
                );
              })
        ],
      ),
      body: FutureBuilder<List<BreedModel>>(
        future: _controller.fetchAll(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.separated(
              separatorBuilder: (context, index) {
                return const SizedBox(
                  height: 20,
                );
              },
              padding: const EdgeInsets.all(20),
              itemCount: snapshot.data!.length,
              itemBuilder: (BuildContext context, int index) {
                final _isFavorite = favorites.any(
                  (e) =>
                      e.name == snapshot.data![index].name &&
                      e.subBreed == snapshot.data![index].subBreed,
                );
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => BreedDetailsScreen(
                          name: snapshot.data![index].name,
                          subBreed: snapshot.data![index].subBreed,
                        ),
                      ),
                    );
                  },
                  child: CustomCardWidget(
                    isFavorite: _isFavorite,
                    onPressed: () {
                      _isFavorite
                          ? _controller.removeFavorite(
                              breed: BreedModel(
                                name: snapshot.data![index].name,
                                subBreed: snapshot.data![index].subBreed,
                              ),
                            )
                          : _controller.saveFavorite(
                              breed: BreedModel(
                                name: snapshot.data![index].name,
                                subBreed: snapshot.data![index].subBreed,
                              ),
                            );
                    },
                    name: snapshot.data![index].subBreed == null
                        ? snapshot.data![index].name
                        : snapshot.data![index].name +
                            ' - ' +
                            snapshot.data![index].subBreed!,
                  ),
                );
              },
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
