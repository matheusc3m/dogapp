import 'package:dogapp/src/controller/home_controller.dart';
import 'package:dogapp/src/model/breed_model.dart';
import 'package:dogapp/src/view/breed_details/breed_details_screen.dart';
import 'package:dogapp/src/view/shared/custom_card_widget.dart';
import 'package:flutter/material.dart';

class FavoritesScreen extends StatefulWidget {
  FavoritesScreen({Key? key}) : super(key: key);

  @override
  State<FavoritesScreen> createState() => _FavoritesScreenState();
}

class _FavoritesScreenState extends State<FavoritesScreen> {
  final _controller = HomeController();
  bool _isFavorite = true;
  late Future<List<BreedModel>> favorites;
  @override
  void initState() {
    super.initState();
    favorites = _controller.getFavorites();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
          ),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
        title: const Text('Favorites'),
        actions: [
          IconButton(
              icon: const Icon(Icons.favorite_border_outlined),
              onPressed: () {})
        ],
      ),
      body: FutureBuilder<List<BreedModel>>(
        future: favorites,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.separated(
              separatorBuilder: (context, index) {
                return const SizedBox(
                  height: 20,
                );
              },
              padding: const EdgeInsets.all(20),
              itemCount: snapshot.data!.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => BreedDetailsScreen(
                          name: snapshot.data![index].name,
                          subBreed: snapshot.data![index].subBreed,
                        ),
                      ),
                    );
                  },
                  child: CustomCardWidget(
                    isFavorite: _isFavorite,
                    onPressed: () {
                      _isFavorite
                          ? [
                              _controller.removeFavorite(
                                breed: BreedModel(
                                  name: snapshot.data![index].name,
                                  subBreed: snapshot.data![index].subBreed,
                                ),
                              ),
                            ]
                          : _controller.saveFavorite(
                              breed: BreedModel(
                                name: snapshot.data![index].name,
                                subBreed: snapshot.data![index].subBreed,
                              ),
                            );
                      _isFavorite = !_isFavorite;
                    },
                    name: snapshot.data![index].subBreed == null
                        ? snapshot.data![index].name
                        : snapshot.data![index].name +
                            ' - ' +
                            snapshot.data![index].subBreed!,
                  ),
                );
              },
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
