import 'package:dogapp/src/controller/breed_details_controller.dart';
import 'package:flutter/material.dart';

class BreedDetailsScreen extends StatelessWidget {
  final String name;
  final String? subBreed;

  final _controller = BreedDetailsController();
  BreedDetailsScreen({Key? key, required this.name, this.subBreed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: FutureBuilder<List<String>>(
        future: _controller.fetchBreedImages(name: name, subBreed: subBreed),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return GridView.builder(
              itemCount: snapshot.data!.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
              ),
              itemBuilder: (BuildContext context, int index) {
                return Image.network(
                  snapshot.data![index],
                  fit: BoxFit.cover,
                );
              },
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
