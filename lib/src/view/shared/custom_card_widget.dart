import 'package:flutter/material.dart';

class CustomCardWidget extends StatefulWidget {
  String name;
  bool isFavorite;
  void Function()? onPressed;

  CustomCardWidget(
      {Key? key,
      required this.name,
      required this.onPressed,
      required this.isFavorite})
      : super(key: key);

  @override
  State<CustomCardWidget> createState() => _CustomCardWidgetState();
}

class _CustomCardWidgetState extends State<CustomCardWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 20.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Container(
          height: 70,
          color: Colors.white,
          child: Row(
            children: <Widget>[
              Container(width: 10, height: 70, color: Colors.red[200]),
              const SizedBox(width: 10),
              Expanded(
                child: Text(widget.name),
              ),
              IconButton(
                onPressed: () {
                  if (widget.onPressed != null) {
                    widget.onPressed!.call();
                    setState(() {
                      widget.isFavorite = !widget.isFavorite;
                    });
                  }
                },
                icon: widget.isFavorite
                    ? const Icon(Icons.favorite, color: Colors.red)
                    : const Icon(Icons.favorite_border_outlined,
                        color: Colors.grey),
              ),
              const SizedBox(width: 10),
            ],
          ),
        ),
      ),
    );
  }
}
