import 'package:dogapp/src/data/dio/custom_dio.dart';
import 'package:dogapp/src/model/breed_model.dart';

class DogCeoRepository {
  final dio = AppDio.getInstance();

  Future<List<BreedModel>> fetchAll() async {
    final response = await dio.get<Map<String, dynamic>>('/breeds/list/all');
    final List<BreedModel> listOfBreeds = [];
    try {
      response.data!['message']!.forEach((key, value) {
        List<dynamic> subBreed = value;
        return subBreed.isEmpty
            ? listOfBreeds.add(BreedModel(name: key))
            : subBreed
                .map(
                    (e) => listOfBreeds.add(BreedModel(name: key, subBreed: e)))
                .toList();
      });

      return listOfBreeds;
    } catch (e) {
      return [];
    }
  }

  Future<List<String>> fetchBreedImages({
    required String name,
    String? subBreed,
  }) async {
    final endpoint = '/breed/$name/images';

    List<String> images = [];
    final response = await dio.get<Map<String, dynamic>>(endpoint);

    try {
      (response.data!['message'] as List<dynamic>)
          .map((e) => images.add(e))
          .toList();
      return images;
    } catch (e) {
      return [];
    }
  }
}
