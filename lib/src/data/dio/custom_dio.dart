import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';

class AppDio with DioMixin implements Dio {
  AppDio._() {
    const baseUrl = 'https://dog.ceo/api/';
    options = BaseOptions(
      baseUrl: baseUrl,
      contentType: 'application/json',
      connectTimeout: 30000,
      sendTimeout: 30000,
      receiveTimeout: 30000,
    );

    httpClientAdapter = DefaultHttpClientAdapter();
  }
  static Dio getInstance() => AppDio._();
}
