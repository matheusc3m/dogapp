import 'dart:convert';

import 'package:dogapp/src/data/repository/dog_ceo_repository.dart';
import 'package:dogapp/src/model/breed_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeController {
  final repository = DogCeoRepository();

  Future<List<BreedModel>> fetchAll() async {
    return await repository.fetchAll();
  }

  void removeFavorite({required BreedModel breed}) async {
    final _prefs = await SharedPreferences.getInstance();
    final favorites = await getFavorites();

    favorites.removeWhere(
      (element) {
        return element.name == breed.name && element.subBreed == breed.subBreed;
      },
    );

    _prefs.setStringList(
      'favorites',
      favorites.map((e) => json.encode(e)).toList(),
    );
  }

  void saveFavorite({required BreedModel breed}) async {
    final favorites = await getFavorites();
    final _prefs = await SharedPreferences.getInstance();
    favorites.add(breed);

    _prefs.setStringList(
      'favorites',
      favorites.map((e) => json.encode(e)).toList(),
    );
  }

  Future<List<BreedModel>> getFavorites() async {
    final _prefs = await SharedPreferences.getInstance();

    final stringList = _prefs.getStringList(
          'favorites',
        ) ??
        [];
    final breeds =
        stringList.map((e) => BreedModel.fromJson(json.decode(e))).toList();
    return breeds;
  }
}
