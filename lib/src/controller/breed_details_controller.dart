import 'package:dogapp/src/data/repository/dog_ceo_repository.dart';

class BreedDetailsController {
  final repository = DogCeoRepository();

  Future<List<String>> fetchBreedImages({
    required String name,
    String? subBreed,
  }) async {
    return await repository.fetchBreedImages(name: name, subBreed: subBreed);
  }
}
